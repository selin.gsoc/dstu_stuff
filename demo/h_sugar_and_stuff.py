# tuple unpacking
one, two = 1, 2
(one, two, three) = ("1", "two", one + two)
first, second, *tail = [1, 2, 3, 4, 5, 6]
*head, last = tail
first, (second1, second2), *tail = [[1, 2], [3, 4], [5, 6]]
# any iterable fits
a, b, *cde = "abcde"

# lambdas
my_replace = lambda s, old, new: new.join(s.split(old))
x = my_replace("11,22,33", ",", " >< ")
print_space_end = lambda *a, **kw: print(*a, end=" ", **kw)
print_space_end(1)
print_space_end(2)
print_space_end(3, 4, 5, sep=":")
print()
x = (lambda: x + "!!!")()
# why do we need those?
# to put somewhere else as an argument
# lets imagine list of facebook developers
devs = [("John", 14), ("Bob", 52), ("Alice", 27), ("Mark", 35)]
# sort them by probability of middle age crisis in Facebook
devs = sorted(devs, key=lambda d: (d[1] - 28) ** 2)
# get names
devs = list(map(lambda d: d[0], devs))

# generating stuff on the fly
# lists
lst = [n ** 2 for n in range(10)]
lst = [n ** 2 for n in range(10) if n % 2]
# dictionaries
dct = {name: len(name) for name in devs}
dct = {v: k for (k, v) in dct.items() if v % 2}
# sets
st = {d + "!" for d in devs}
# parse input
input_seq = "1,2,3,    4,  5, 6, 42"
g = (int(n.strip()) for n in input_seq.split(","))
x = type(g)
x = tuple(g)
pass
