# <remainder>


def decorator(fn):
    return f"Here was function {fn.__name__}"


def myfunc():
    pass


myfunc = decorator(myfunc)


@decorator
def myfunc2():
    pass


# </remainder>

class DemoDad:
    def normal_method(self, someparam):
        return "Normal method result " + repr((self, someparam))

    @classmethod
    def class_method(cls, someparam):
        return "Class method result " + repr((cls, someparam))

    @staticmethod
    def static_method(someparam):
        return "Static method result " + repr(someparam)

    def __init__(self):
        self.answer = 42

    @property
    def some_property(self):
        return "Some property value " + repr(self.answer)

    @some_property.setter
    def some_property(self, newval):
        self.answer = newval


demo_dad = DemoDad()
x = demo_dad.normal_method(1)
y = DemoDad.normal_method(2, 3)
x = demo_dad.class_method("lol")
y = DemoDad.class_method("kek")
x = demo_dad.static_method("lol")
y = DemoDad.static_method("kek")
x = demo_dad.some_property
y = DemoDad.some_property
y = type(y)
demo_dad.some_property = "trash"
x = demo_dad.some_property


class DemoKid(DemoDad):
    pass


y = DemoKid.class_method("kek")


# More "realistic" example (but not really)

class Shape:
    @property
    def volume(self):
        raise NotImplementedError("Volume is not implemented")


try:
    Shape().volume
except Exception as e:
    pass


class Cuboid(Shape):
    def __init__(self, width, height, depth):
        self.dimensions = sorted([width, height, depth])

    @property
    def volume(self):
        d = self.dimensions
        return d[0] * d[1] * d[2]


try:
    x = Cuboid(2, 3, 7)
    x.bolume = x.volume
    x.volume = x.bolume
except Exception as e:
    pass


class Body:
    def __init__(self, density, shape):
        self.density = density
        self.shape = shape

    @classmethod
    def from_volume(cls, density, volume):
        # Build some default shape
        side = volume ** (1 / 3)
        shape = Cuboid(side, side, side)
        return cls(density, shape)

    @property
    def mass(self):
        return self.density * self.shape.volume

    @mass.setter
    def mass(self, newmass):
        self.density = newmass / self.shape.volume


b = Body.from_volume(1000, 27)
x = b.shape.dimensions

x = b.mass, b.density
b.mass = b.mass / 2
x = b.mass, b.density


class Person:
    def __init__(self, name, age):
        self.name, self.age = name, age
    

class Employee(Person):
    def __init__(self, name, age, company):
        Person.__init__(self, name, age)
        self.company = company
x = Employee("J.Johnson", 33, "Horns&Hooves").__dict__

class Employee(Person):
    def __init__(self, name, age, company):
        super(Employee, self).__init__(name, age)
        self.company = company
x = Employee("J.Johnson", 34, "Horns&Hooves").__dict__


class Employee(Person):
    def __init__(self, name, age, company):
        super().__init__(name, age)  # super() without parameters is nothing but dark interpreter magic
        self.company = company
x = Employee("J.Johnson", 35, "Horns&Hooves").__dict__


class A:
    kek = "A"


class B(A):
    kek = "B"


class C(A):
    kek = "C"


class D(B, C):
    kek = "D"


b, c, d = B(), C(), D()
x = super(B, b).kek
x = super(C, c).kek
x = super(D, d).kek
x = super(B, d).kek
# searching attributes in a class after C in d.__class__.__mro__
x = super(C, d).kek


pass
