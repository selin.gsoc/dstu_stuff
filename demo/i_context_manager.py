import time


def format_msg(msg):
    return "{}: {}\r\n".format(
        time.strftime("%Y-%m-%d-%H.%M.%S", time.localtime()), msg)


def squalid_log(msg):
    f = open("log.txt", "a")
    try:
        f.write(format_msg(msg))
    finally:
        f.close()


squalid_log("==========SESSION STARTED!=========")
squalid_log("hey!")
squalid_log("what's up!")


def squalid_log(msg):
    with open("log.txt", "a") as f:
        f.write(format_msg(msg))


squalid_log("still alive!")


class ContextManagerDemo:
    def __enter__(self):
        squalid_log("Running __enter__ for {}".format(self))
        junk = f"junk_{time.time()}"
        squalid_log(f"Returning {junk}")
        return junk

    def __exit__(self, type, value, traceback):
        squalid_log("Running __exit__ for {}".format(
            (self, type, value, traceback)))
        # Returning True: the exception is gracefully handled.
        # Returning False: the exception re-reasing.
        return type is not None and issubclass(type, ArithmeticError) 


with ContextManagerDemo() as garbage:
    squalid_log(garbage) 

try:
    with ContextManagerDemo():
        1 / 0
except Exception as e:
    squalid_log(f"Error occured {e}")
else:
    squalid_log(f"Everything is fine")

try:
    with ContextManagerDemo():
        raise FileNotFoundError
except Exception as e:
    squalid_log(f"Error occured {e}")
else:
    squalid_log(f"Everything is fine")