
golden_card = {"owner": "J.Bergstein",
               "rank": "golden",
               "points": 0}

bronze_card = {"owner": "A.White",
               "rank": "bronze",
               "points": 0}


def apply_points(card, price):
    points_used = card["points"] if card["points"] < price else price
    card["points"] -= points_used
    return price - points_used


def apply_sale(card, price):
    if card["rank"] == "bronze":
        sale = 0.02
    elif card["rank"] == "silver":
        sale = 0.05
    elif card["rank"] == "golden":
        sale = 0.1
    else:
        sale = 0
    return price * (1 - sale)


def score_points(card, price):
    if card["rank"] == "bronze":
        point_factor = 0.02
    elif card["rank"] == "silver":
        point_factor = 0.03
    elif card["rank"] == "golden":
        point_factor = 0.05
        if price >= 10000:
            point_factor *= 2
    else:
        point_factor = 0.01
    card["points"] += price * point_factor


def transact(card, initial_price, use_points=False):
    final_price = initial_price
    if use_points:
        final_price = apply_points(card, final_price)
    final_price = apply_sale(card, final_price)
    score_points(card, final_price)
    return final_price


def print_cashiers_check(price, card, use_points=False):
    print("=" * 20)
    print("Total price:")
    print(transact(card, price, use_points))
    if card["rank"] == "golden":
        print("Have a nice day")
        print(card["owner"])
    print("=" * 20)


print_cashiers_check(150, golden_card)
print_cashiers_check(15000, golden_card)
print_cashiers_check(150, golden_card, use_points=True)
print_cashiers_check(100, bronze_card)

broken_card = {"lol": "no"}
try:
    print_cashiers_check(100, broken_card)
except Exception as e:
    pass


def create_card(owner, rank):
    return {
        "owner": owner,
        "rank": rank,
        "points": 0
    }


print_cashiers_check(100, create_card("J.K.Chan", "silver"))
# still could screw up assigning rank

# What if we want to add platinum card? if/else apocalypse
# Solution to group if/else in one bundle?

# We will re-define all functions, and remove old ones just so we catch an exception if we forget something
del create_card
del print_cashiers_check
del transact
del apply_sale
# del apply_points  - doesn't change
del score_points


def create_plebian_card(owner):
    card = {
        "owner": owner,
        "rank": None,
        "points": 0,
        "sale": 0,
        "point_factor": 0.01,
        # add available actions on cards
        "apply_points": apply_points,
        "apply_sale": apply_sale,
        "score_points": score_points,
        "transact": transact
    }
    return card


def apply_sale(card, price):
    return price * (1 - card["sale"])


def score_points(card, price):
    card["points"] += price * card["point_factor"]


def transact(card, initial_price, use_points=False):
    final_price = initial_price
    if use_points:
        final_price = card["apply_points"](card, final_price)
    final_price = card["apply_sale"](card, final_price)
    card["score_points"](card, final_price)
    return final_price


def print_cashiers_check(price, card, use_points=False):
    print("=" * 20)
    print("Total price:")
    print(card["transact"](card, price, use_points))
    if card["rank"] == "golden":
        print("Have a nice day")
        print(card["owner"])
    print("=" * 20)


plebian = create_plebian_card("Simpleton")
print_cashiers_check(150, plebian)
print_cashiers_check(15000, plebian)
print_cashiers_check(150, plebian, use_points=True)


def create_bronze_card(owner):
    card = create_plebian_card(owner)
    card["rank"] = "bronze"
    card["sale"] = 0.02
    card["point_factor"] = 0.02
    return card


bronze = create_bronze_card("E.R.Normalguy")
print_cashiers_check(150, bronze)
print_cashiers_check(15000, bronze)
print_cashiers_check(150, bronze, use_points=True)


def create_silver_card(owner):
    card = create_plebian_card(owner)
    card["rank"] = "silver"
    card["sale"] = 0.05
    card["point_factor"] = 0.03
    return card


silver = create_silver_card("J.Wealthy")
print_cashiers_check(150, silver)
print_cashiers_check(15000, silver)
print_cashiers_check(150, silver, use_points=True)


def score_points_golden(card, price):
    point_factor = card["point_factor"]
    if price >= 10000:
        point_factor *= 2
    card["points"] += price * point_factor


def create_golden_card(owner):
    card = create_plebian_card(owner)
    card["rank"] = "golden"
    card["sale"] = 0.1
    card["point_factor"] = 0.05
    card["score_points"] = score_points_golden
    return card


golden = create_golden_card("L.R.Hampton III")
print_cashiers_check(150, golden)
print_cashiers_check(15000, golden)
print_cashiers_check(150, golden, use_points=True)


class Card:

    def __init__(self, owner):
        self.owner = owner
        self.sale = 0.0
        self.point_factor = 0.01
        self.points = 0

    def apply_points(self, price):
        points_used = self.points if self.points < price else price
        self.points -= points_used
        return price - points_used

    def apply_sale(self, price):
        return price * (1 - self.sale)

    def score_points(self, price):
        self.points += price * self.point_factor

    def transact(self, initial_price, use_points=False):
        final_price = initial_price
        if use_points:
            final_price = self.apply_points(final_price)
        final_price = self.apply_sale(final_price)
        self.score_points(final_price)
        return final_price


class BronzeCard(Card):

    def __init__(self, owner):
        Card.__init__(self, owner)
        self.sale = 0.02
        self.point_factor = 0.02


class SilverCard(Card):

    def __init__(self, owner):
        Card.__init__(self, owner)
        self.sale = 0.05
        self.point_factor = 0.03


class GoldenCard(Card):

    def __init__(self, owner):
        Card.__init__(self, owner)
        self.sale = 0.1
        self.point_factor = 0.05

    def score_points(self, price):
        point_factor = self.point_factor
        if price >= 10000:
            point_factor *= 2
        self.points += price * point_factor


def print_cashiers_check(price, card=None, use_points=False):
    if card is None:
        card = Card("")
    print("=" * 20)
    print("Total price:")
    print(card.transact(price, use_points))
    if isinstance(card, GoldenCard):
        print("Have a nice day")
        print(card.owner)
    print("=" * 20)


golden_card = GoldenCard("J.Bergstein")
print_cashiers_check(150, golden_card)
print_cashiers_check(15000, golden_card)
print_cashiers_check(150, golden_card, use_points=True)
bronze_card = BronzeCard("A.White")
print_cashiers_check(150, bronze_card)
print_cashiers_check(150)


x = type(golden_card)
x = x == GoldenCard

x = type(42)
x = type(42.0)
x = type(True)
x = type({})
x = type(())


def some_function():
    pass


x = type(some_function)

x = type("something") == str
x = type([]) == list

# What are these 'types'?
x = str.split("apple, orange, pear", ", ")
x = str.join(" <|=|> ", x)

x = "apple, orange, pear".split(", ")
x = " ? ".join(x)

x = {"one": 1,
     2: "two"}

x.update({"three": 3})
update_x = x.update
update_x({4: "four"})

x = dict()
x = dict(apple="green", orange="orange")
x = dict(x)
x = int("42")
x = str(x)


class Demo:
    one = 42
    two = "heyho"

    def three(self):
        return self


x = Demo.one
x = Demo.two
x = Demo.three
demo = Demo()
x = Demo.three(demo) is demo
x = demo.three() is demo

x = Demo
x = x.one


class Demo2(Demo):
    two = "lol"


x = Demo2.one
x = Demo2.two
demo2 = Demo2()
x = demo2.three() is demo2


class Shape:
    def volume(self):
        raise NotImplementedError("Volume is not implemented")


x = Shape()
try:
    x.volume()
except Exception as e:
    pass


class Cuboid(Shape):
    def __init__(self, width, height, depth):
        self.dimensions = sorted([width, height, depth])

    def volume(self):
        d = self.dimensions
        return d[0] * d[1] * d[2]

    def __eq__(self, other):
        return self.dimensions == other.dimensions


x = Cuboid(1, 2, 3).volume()
x = Cuboid(1, 2, 3) == Cuboid(1, 3, 2)
x = Cuboid(1, 2, 3) == Cuboid(1, 3, 3)


class Cylinder(Shape):
    def __init__(self, radius, height):
        self.radius = radius
        self.height = height

    def volume(self):
        pi = 3.1415
        return pi * self.radius ** 2 * self.height


class Body(Shape):
    def __init__(self, density):
        self.density = density

    def mass(self):
        return self.density * self.volume()


try:
    Body(density=1000).mass()
except Exception as e:
    pass


class Brick(Cuboid, Body):
    def __init__(self, width, height, depth, density):
        Cuboid.__init__(self, width, height, depth)
        Body.__init__(self, density)


x = Brick(1, 2, 3, 1000).mass()


class Pillar(Cylinder, Body):
    def __init__(self, radius, height, density):
        Cylinder.__init__(self, radius, height)
        Body.__init__(self, density)


x = Pillar(1, 1, 1000).mass()

x = Pillar.__mro__
x = Brick.__mro__

brick = Brick(1, 2, 3, 1000)
x = isinstance(brick, type(brick))
x = isinstance(brick, Brick)
x = isinstance(brick, Body)
x = isinstance(brick, Shape)
x = isinstance(brick, Pillar)
x = isinstance(brick, str)

x = isinstance(brick, object)

x = isinstance({}, object)
x = isinstance([1, 2], object)
x = isinstance(42, object)
x = isinstance("qwe", object)

# <hard_stuff>
x = type(brick)
x = type(brick) is Brick
x = type(Brick)
x = type(object)
x = type(type)

x = isinstance(brick, object)
x = isinstance(Brick, object)
x = isinstance(type, object)
x = isinstance(object, object)


# </hard_stuff>


class Odd:
    def __init__(self, n):
        self.n = n

    def __bool__(self):
        return self.n % 2 == 1


x = bool(Odd(1))
x = bool(Odd(2))
x = bool(Odd(3))
x = Odd(4)
if x:
    x = "123"


class MyRange:
    def __init__(self, start, end, step=1):
        self.params = start, end, step

    def __iter__(self):
        return MyRangeIter(*self.params)


class MyRangeIter:
    def __init__(self, start, end, step):
        self.current = start
        self.end = end
        self.step = step

    def __next__(self):
        if self.current < self.end:
            r = self.current
            self.current += self.step
            return r
        else:
            raise StopIteration()


print("=" * 40)
for x in MyRange(-5, 6, 2):
    print(x)
print("=" * 40)


# P.S Almost nobody actually uses multiple inheritance if there are
class SaneBody:
    def __init__(self, shape, density):
        self.shape = shape
        self.density = density

    def mass(self):
        return self.shape.volume() * self.density

    def volume(self):
        # Even if we do need a volume we implement it like this
        return self.shape.volume()


class SaneBrick(SaneBody):
    def __init__(self, width, height, depth, density):
        SaneBody.__init__(self, Cuboid(width, height, depth), density)


brick = SaneBrick(1, 2, 3, 1000)
x = brick.mass()
x = brick.volume()
# Way we go in case we would not implement SaneBrick.volume()
x = brick.shape.volume()

pass
