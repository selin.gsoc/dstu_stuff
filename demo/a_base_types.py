# int and float
int_sum = 123 + 222
# assignment is not equality
int_sum = int_sum + 1
int_sum += 1
float_mul = 1.5 * 4
power_of_two = 2 ** 10
division = 3 / 2
integer_division = 3 // 2
residual = 3 % 2

# строки
hello_string = 'Hello'
hello_string = "Hello"
hello_string_length = len(hello_string)
letter_h = hello_string[0]
letter_l = hello_string[2]
letter_o = hello_string[hello_string_length - 1]
still_letter_o = hello_string[-1]
el_string = hello_string[1:3]
ello_string = hello_string[1:]
hell_string = hello_string[:4]
another_hello_string = hello_string[:]
hello_world_string = hello_string + ", wo" + "rld"
hello_world_string += "!!"
ajk_string = "AJK \x41\x4a\x4b A\x4aK"
ajk_string2 = r"AJK \x41\x4a\x4b A\x4aK"

try:
    hello_string[0] = "A"
except Exception as e:
    pass

# байты
some_bytes = b"123\x01\x02\x03\x41"
x = some_bytes[0:4]


# списки
some_list = [1, 3, 2, -1, 0]
some_list = [42]
some_list = []
some_list = [1, "hi", [4, 5], "there"]
five = some_list[2][1]
some_list = [10, 20, 30, 40, 50, 60, 70]
some_list = some_list[1: -1]
try:
    some_list[len(some_list)]
except Exception as e:
    pass
some_list[0] = 80


# словари
some_dict = {"answer": 42, "apple": "green", 42: 5051704}
x = some_dict["answer"]
x = some_dict[x]
some_dict["apple"] += " world"
some_dict = {}
some_dict['answer'] = 42
some_dict[42] = 5051704
some_dict[42] = {"hey": "ho"}
some_dict[42] = [1, 2, 3]


# кортежи
some_tuple = (-2, -1, "zero", 1, 2)
x = some_tuple[2]
some_tuple = (-3,) + some_tuple + (3,)
some_tuple = ()
some_tuple = 1, 2, 3, 4
try:
    some_tuple[2] = 33
except Exception as e:
    pass


# множества
some_set_1 = {"one", 2, 3, 4}
some_set_2 = {4, "one", 4.2}
x = some_set_1 & some_set_2
x = some_set_1 | some_set_2
x = some_set_1 - some_set_2


# булев тип (истина/ложь)
some_bool = True
some_bool = False
some_bool = not some_bool
some_bool = not some_bool
some_bool = True and False
some_bool = True or False
some_bool = 42.1 <= 42
some_bool = 42 <= 42.1 <= 43
some_bool = 42 in {41, "42"}
some_bool = 42 in [42] and "42" in "1422"

# общие замечания
d1 = {"one": 1, "two": 2}
d2 = d1
d3 = {"one": 1, "two": 2}
x = d1 == d2
x = d1 == d3
x = d1 is d2
x = d1 is d3
d1["three"] = 3
x = d1 == d2
x = d1 == d3

pass
