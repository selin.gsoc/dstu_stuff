if __name__ == "__main__":
    print("module was launched")
else:
    print("module was imported")
print("module name: " + __name__)


text = "lol"


def side_effect():
    # no effect on global text
    text = "lal"


side_effect()


# This function changes the external context, it may have unobvious effect.
def side_effect():
    global text
    text = "lal"


side_effect()


# Pure functions only transform(proccess) data(haven't unobvious effect)
def pure_function(data):
    new_data = data + "lol"
    return new_data
