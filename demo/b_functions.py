external_variable = 1

def some_function(x1, x2):
    x1 += external_variable
    return x1, x2
    x2 += external_variable
    return x1, x2

x = some_function(10, 20)
x = some_function(10, x2=20)
x = some_function(x1=10, x2=20)
x = some_function(x2=10, x1=20)
# x = some_function(x1=10, 20)  # invalid syntax

try:
    x1
except Exception as e:
    pass

def some_function():
    hi = "hi!"
    print(hi)

x = some_function()

def some_function():
    hi = "hi!"
    print(hi)
    return None

x = some_function()

try:
    hi
except Exception as e:
    pass

def some_function(x1, x2=20, x3="auch!"):
    return x1, x2, x3

x = some_function(10)
x = some_function(10, "hey", "you")
x = some_function(10, x3="you")

def some_function(x1, *, x2):
    return x1, x2

try:
    x = some_function(10, 30)
except Exception as e:
    pass

x = some_function(10, x2=30)

def some_function(xx, yy, *args):
    return [xx, yy, args]

x = some_function(10, 20, 30, "ok", False)
x = some_function(10, 20)
x = some_function(10, 20, *[30, "ok", False])
x = some_function(*[10, 20, 30, "ok", False])
x = some_function(*[10, 20])
arguments = ["ho "] * 5
x = some_function(*arguments)

def some_function(**kwargs):
    return kwargs

x = some_function(one="hi", two=42, three=True)
x = some_function()
keyword_arguments = {
    "one": "hi", "two": 42, "three": True
}
x = some_function(**keyword_arguments)

def some_function(a, b=42, c="hello", *positional, d=True, e, **keyword):
    return {
        "a": a,
        "b": b,
        "c": c,
        "positional": positional,
        "d": d,
        "e": e,
        "keyword": keyword
    }

x = some_function(888, c="hi", e=[1, 2, 3], kojima="genius")
x = some_function(1, 2, 3, 4, 5, e=None, d=False)

def add_prefix(f):
    msg = "Result=" + f()
    return msg

def answer():
    return "42"

x = add_prefix(answer)

def add_prefix_decorator(f):
    def new_f():
        return add_prefix(f)
    return new_f

answer = add_prefix_decorator(answer)
x = answer()

@add_prefix_decorator
def answer():
    return "forty two"

x = answer()

pass
