x = 8
if x < 10:
    x = 15
    x += 1
    l = [1, 2, 3]

if x < 10:
    x = 100
    l = []

if True:
    x = 0

if False:
    x = 1

x = x < 10
x = x is x
x = x == x

if 13:
    x = 0

if 0:
    x = 13

if "abc":
    x = []

if x:
    x = "123"


x = bool(False)
x = bool(13)
x = bool(-13)
x = bool(0)
x = bool("hooray!")
x = bool("")
x = bool([0])
x = bool({})


def kinetic_energy(mass, velocity=None,
                   acceleration=None, initial_velocity=0,
                   time_delta=None, distance_delta=None):
    if velocity is not None:
        return mass * velocity ** 2 / 2
    if acceleration is None:
        raise Exception("Neither a velocity nor an acceleration provided")
    if time_delta is not None:
        velocity = initial_velocity + acceleration * time_delta
        return mass * velocity ** 2 / 2
    if distance_delta is not None:
        return mass * (initial_velocity ** 2 + acceleration * distance_delta)
    raise Exception("Neither a time_delta nor a distance_delta provided")

x = kinetic_energy(1, velocity=10)
x = kinetic_energy(1, 10)
try:
    x = kinetic_energy(1)
except Exception as e:
    pass
x = kinetic_energy(1, acceleration=9.8, time_delta=2)
x = kinetic_energy(1, acceleration=9.8, distance_delta=2)
x = kinetic_energy(1, acceleration=9.8, distance_delta=2, initial_velocity=1)
x = kinetic_energy(1, None, 9.8, 1, None, 2)
try:
    x = kinetic_energy(1, acceleration=9.8)
except Exception as e:
    pass


def drunken_irish_number(n):
    if 0 <= n <= 3:
        w = "a few"
    else:
        w = "a lot"
    print(w)
    return w

x = drunken_irish_number(1)
x = drunken_irish_number(3)
x = drunken_irish_number(15)


def drunken_irish_number(n):
    if 0 <= n <= 3:
        if n == 0:
            w = "nothhheng!"
        else:
            w = "a few"
    else:
        w = "a lot"
    print(w)
    return w

x = drunken_irish_number(0)
x = drunken_irish_number(3)
x = drunken_irish_number(15)


def drunken_irish_number(n):
    if n == 0:
        w = "zero"
    elif n == 1:
        w = "one"
    elif n == 2:
        w = "two"
    elif n == 3:
        w = "three"
    else:
        w = str(n)
    print(w)
    return w

x = drunken_irish_number(0)
x = drunken_irish_number(1)
x = drunken_irish_number(3)
x = drunken_irish_number(15)


x = 15
x = x if x < 0 else -x
x = "negative" if x < 0 else "non negative"

x = (1 if False else 2) if True else 3
x = 1 if False else (2 if True else 3)
# don't write like this in real life
x = 1 if False else 2 if True else 3

din = drunken_irish_number
# only 'three' printed to console
x = din(1) if False else din(2) if True else din(3)

# printed to console 'one', 'two' and 'three'
x = {
    True: din(1),
    False: {
        True: din(2),
        False: din(3)
    }[True]
}[False]


x = ["zero", "one", "two", "tree", "four", "five"]
i = 0
s = ""
while i < len(x):
    s += x[i] + "; "
    i += 1

# lets find even element index
x = [1, 3, 5, 6, 7]
i = 0
while i < len(x):
    if x[i] % 2 == 0:
        break
    i += 1
# i - index of even element

# what if there is no even
x = [1, 3, 5, 7]
i = 0
while i < len(x):
    if x[i] % 2 == 0:
        break
    i += 1
else:
    i = None


def for_demo(iterable):
    print("=" * 40)
    print("'For' demo of {}:".format(iterable))

    for x in iterable:
        print(x)

    print("=" * 40)

for_demo([1, 2, "three", 4.0])
for_demo((1, 2, "three", 4.0))
for_demo({1, 2, "three", 4.0})
for_demo({1: 2, "three": 4.0})
for_demo("three")


# find first even element (element itself, not index)
x = [1, 3, 5, 6, 7]
for elem in x:
    if elem % 2 == 0:
        break
print(elem)

# what if there are no even elements ?
x = [1, 3, 5, 7]
for elem in x:
    if elem % 2 == 0:
        break
else:
    elem = None
print(elem)


pass
