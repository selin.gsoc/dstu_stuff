class MyDescriptor:

    def __get__(self, instance, owner):
        return repr((self, instance, owner))

    def __set__(self, instance, value):
        print("Tried to set: ", self, instance, value)


class SomeClass:
    my_descriptor = MyDescriptor()


x = SomeClass().my_descriptor
SomeClass().my_descriptor = 42
x = SomeClass.my_descriptor
SomeClass.my_descriptor = 42
x = SomeClass.my_descriptor


class Empty:
    name = "empty"


empty = Empty()


def explore(*args, **kwargs):
    return args, kwargs


x = property(explore).__get__(empty, Empty)
x = classmethod(explore).__get__(empty, Empty)(42)
x = staticmethod(explore).__get__(empty, Empty)(42)
x = explore.__get__(empty, Empty)(42)  # this is how methods work


class Empty2:
    name = "empty2"


class MergedEmpties(Empty, Empty2):
    sup = super(Empty)


merged_empties = MergedEmpties()
x = merged_empties.sup.name

# even this thing supports protocol
x = super(MergedEmpties).__get__(merged_empties, MergedEmpties).name


class MyClassMethod:
    def __init__(self, f):
        self.f = f

    def __get__(self, instance, owner):
        def new_f(*args, **kwargs):
            return self.f(owner, *args, **kwargs)
        return new_f


class BlaBla:
    e1 = classmethod(explore)
    e2 = MyClassMethod(explore)


class BlaBla2(BlaBla):
    pass


x = BlaBla.e1(1, 2, 3) == BlaBla.e2(1, 2, 3)
x = BlaBla().e1(1, 2, 3) == BlaBla().e2(1, 2, 3)
x = BlaBla2.e1(1, 2, 3) == BlaBla2.e2(1, 2, 3)
x = BlaBla2().e1(1, 2, 3) == BlaBla2().e2(1, 2, 3)
pass
