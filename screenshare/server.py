from socket import socket
from threading import Thread
from functools import partial
import pickle

from mss import mss
import pygame

import transport


class ScreenShooter:
    @staticmethod
    def screenshots(top, left, width, height):
        rect = {'top': top, 'left': left,
                'width': width, 'height': height}
        with mss() as sct:
            while True:
                img = sct.grab(rect)
                yield img.rgb

    def retreive_screenshots_routine(self, callback):
        clock = pygame.time.Clock()
        for s in self.screenshots(self.top, self.left,
                                  self.width, self.height):
            callback(s)
            clock.tick(40)

    def handle_new_screenshot(self, screenshot: bytes):
        if screenshot == self.last_shot:
            return
        self.last_shot = screenshot

        for s in self.subscribers:
            s(self)

    def subscribe(self, callback):
        self.subscribers.append(callback)

    def unsubscribe(self, callback):
        self.subscribers.remove(callback)

    def is_subscribed(self, callback):
        return callback in self.subscribers

    def __init__(self, top=0, left=0, width=0, height=0):
        pygame.init()
        if width == 0 or height == 0:
            display_info = pygame.display.Info()
            width = width if width else display_info.current_w - left
            height = height if height else display_info.current_h - top
        self.top, self.left = top, left
        self.width, self.height = width, height
        self.routine_thread = Thread(target=self.retreive_screenshots_routine,
                                     args=(self.handle_new_screenshot,))
        self.routine_thread.start()
        self.last_shot = None
        self.subscribers = []


def broadcast_screenshot(conn: socket, ss: ScreenShooter):
    to_send_data = pickle.dumps((ss.width, ss.height, ss.last_shot))
    try:
        transport.format_send(conn, to_send_data)
    except Exception as e:
        if ss.is_subscribed(broadcast_screenshot):
            ss.unsubscribe(broadcast_screenshot)


def main(host="0.0.0.0", port=5000):
    port = int(port)
    ss = ScreenShooter()
    sock = socket()
    sock.bind((host, port))
    try:
        sock.listen(20)
        print('Server started.')

        while 'connected':
            conn, addr = sock.accept()
            print('Client connected IP:', addr)
            ss.subscribe(partial(broadcast_screenshot, conn))
    finally:
        sock.close()


if __name__ == '__main__':
    import sys
    _, *args = sys.argv
    main(*args)