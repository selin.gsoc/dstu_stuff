from socket import socket
import pickle
import pygame
import transport



def main(host='127.0.0.1', port=5000):
    port = int(port)
    pygame.init()
    display_info = pygame.display.Info()
    dspl_w = display_info.current_w
    dspl_h = display_info.current_h
    screen = pygame.display.set_mode((dspl_w, dspl_h), pygame.RESIZABLE)
    clock = pygame.time.Clock()

    sock = socket()
    sock.connect((host, port))
    try:
        watching = True
        while watching:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    watching = False
                    break
                elif event.type == pygame.VIDEORESIZE:
                    dspl_w, dspl_h = event.w, event.h

            received_data = transport.format_recv(sock)
            if not received_data:
                continue
            img_w, img_h, pixels = pickle.loads(received_data)

            img = pygame.image.fromstring(pixels, (img_w, img_h), 'RGB')
            if img_w != dspl_w or img_h != dspl_h:
                img = pygame.transform.scale(img, (dspl_w, dspl_h))

            # Display the picture
            screen.blit(img, (0, 0))
            pygame.display.flip()
            clock.tick(40)
    finally:
        sock.close()


if __name__ == '__main__':
    import sys
    _, *args = sys.argv
    main(*args)