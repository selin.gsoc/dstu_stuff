from time import sleep
from . import api
from ..abstract import AbstractInterface

TELEGRAM_BOT_TOKEN = "865810054:AAHRyqBps7La_GDD8AI_VvmfFSEreXRimJs"
# use scheme socks5h instead of socks5 to enable
# hostname resolution on the other side of the proxy
# (resolves on our side by default)
PROXY = "socks5h://127.0.0.1:9150"


class TelegramInterface(AbstractInterface):
    tapi = api.TelegramBotApi(TELEGRAM_BOT_TOKEN, PROXY)

    def __init__(self, callback):
        super(TelegramInterface, self).__init__(callback)

    def process_update(self, update):
        message = update["message"]
        result = self.callback(message["text"])
        self.tapi.send_message(message["chat"]["id"], result)
        return update["update_id"]

    def run(self):
        max_update_id = 0
        while True:
            try:
                updates = self.tapi.get_updates(offset=max_update_id + 1,
                                                timeout=30)["result"]
                for update in updates:
                    update_id = self.process_update(update)
                    max_update_id = max(max_update_id, update_id)
            except KeyboardInterrupt:
                break
            sleep(1)



