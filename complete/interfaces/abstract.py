import traceback


class AbstractInterface:
    def __init__(self, callback):
        self._callback = callback

    def callback(self, text):
        try:
            return str(self._callback(text))
        except Exception:
            return traceback.format_exc()

    def run(self):
        raise NotImplementedError("You need to implement this method in a descendant")
